# -*- mode: ruby -*-
# vi: set ft=ruby :

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
mv  = ''
sdv = ''

if ARGV[0] == 'up' && (Dir.glob("#{File.dirname(__FILE__)}/.vagrant/machines/default/virtualbox/*").empty? || ARGV[1] == '--provision')
  mvs = ["1.9.3.2", "1.9.3.1", "1.9.3.0", "1.9.2.4", "1.9.2.3", "1.9.2.2", "1.9.2.0", "1.9.1.1", "1.9.1.0", "1.9.0.1", "1.8.1.0", "1.8.0.0", "1.7.0.2", "1.7.0.1", "1.7.0.0", "1.6.1.0"]
  mv  = mvs[0]
  i   = 0
  for v in mvs
    i += 1
    print(i, ") ", v, "\n");
  end
  print("Please choose the Magento version [1-", mvs.length, "]: ")
  input = Integer(STDIN.gets)
  if input > 0 && input <= mvs.length
    mv = mvs[input - 1]
  else
    print "Your choice is out of range!\n"
    exit(-1)
  end
  
  print "Do you need the sample data? [y/n]: "
  input = STDIN.gets.chomp
  if input == 'y' || input == 'Y'
    if mv >= '1.9.2.4'
      sdv = '1.9.2.4'
    elsif mv >= '1.9.1.0'
      sdv = '1.9.1.0'
    elsif mv >= '1.9.0.0'
      sdv = '1.9.0.0'
    else
      sdv = '1.6.1.0'
    end
  end
  print "\n"
end
Vagrant.configure(2) do |config|
  # The most common configuration options are documented and commented below.
  # For a complete reference, please see the online documentation at
  # https://docs.vagrantup.com.

  # Every Vagrant development environment requires a box. You can search for
  # boxes at https://atlas.hashicorp.com/search.
  config.vm.box = "paliarush/magento2.ubuntu"

  # Disable automatic box update checking. If you disable this, then
  # boxes will only be checked for updates when the user runs
  # `vagrant box outdated`. This is not recommended.
  # config.vm.box_check_update = false

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine. In the example below,
  # accessing "localhost:8080" will access port 80 on the guest machine.
  # config.vm.network "forwarded_port", guest: 80, host: 8080

  # Create a private network, which allows host-only access to the machine
  # using a specific IP.
  config.vm.network "private_network", ip: "192.168.69.69"

  # Create a public network, which generally matched to bridged network.
  # Bridged networks make the machine appear as another physical device on
  # your network.
  # config.vm.network "public_network"

  # Share an additional folder to the guest VM. The first argument is
  # the path on the host to the actual folder. The second argument is
  # the path on the guest to mount the folder. And the optional third
  # argument is a set of non-required options.
  # config.vm.synced_folder "./src/", "/var/www/l.dev/web/", type:"rsync", create: true, rsync__args:["--verbose", "--archive", "-z", "--copy-links"] 

  # Provider-specific configuration so you can fine-tune various
  # backing providers for Vagrant. These expose provider-specific options.
  # Example for VirtualBox:
  #
  config.vm.provider "virtualbox" do |vb|
  #   # Display the VirtualBox GUI when booting the machine
  #   vb.gui = true
  #
  #   # Customize the amount of memory on the VM:
     vb.cpus = "2"
     vb.memory = "4096"
  end

  #
  # View the documentation for the provider you are using for more
  # information on available options.

  # Define a Vagrant Push strategy for pushing to Atlas. Other push strategies
  # such as FTP and Heroku are also available. See the documentation at
  # https://docs.vagrantup.com/v2/push/atlas.html for more information.
  # config.push.define "atlas" do |push|
  #   push.app = "YOUR_ATLAS_USERNAME/YOUR_APPLICATION_NAME"
  # end

  # Enable provisioning with a shell script. Additional provisioners such as
  # Puppet, Chef, Ansible, Salt, and Docker are also available. Please see the
  # documentation for more information about their specific syntax and use.
  #config.vm.provision :shell, :path => "install.sh", :args => [mv, sdv]
  config.vm.provision "shell", args: [mv, sdv], inline: <<-SHELL
    #!/bin/bash
    MV=$1;  # MAGENTO VERSION
    SDV=$2; # SAMPLE DATA VERSION

    SOURCE_BASE_PATH="https://bitbucket.org/anaraky/mage1de/downloads/";

    if [ "1.9.3.2" \> $MV ]
    then
      a2dismod php7.0;
      a2enmod php5.6;
    fi

    apt-get install unzip;
    mkdir -p "/var/www/l.dev/web";
    echo "#L.DEV#<br><?php echo phpinfo(); ?>" > /var/www/l.dev/web/index.php
    mkdir -p "/var/www/l.dev/logs";
    echo -e "<VirtualHost *:80>\n  ServerName l.dev\n  ServerAlias www.l.dev\n  ServerAdmin webmaster@localhost\n  DocumentRoot /var/www/l.dev/web\n  <Directory /var/www/l.dev/web/>\n    DirectoryIndex index.php\n    Options Indexes FollowSymLinks MultiViews\n    AllowOverride All\n    Order allow,deny\n    allow from all\n  </Directory>\n  ErrorLog /var/www/l.dev/logs/error.log\n  CustomLog /var/www/l.dev/logs/access.log combined\n</VirtualHost>" > /etc/apache2/sites-available/l.dev.conf;
    chmod -R 0777 "/etc/apache2/sites-available/l.dev.conf";
    ln -s /etc/apache2/sites-available/l.dev.conf /etc/apache2/sites-enabled/l.dev.conf;
    cd /var/www/l.dev/web/;
    echo "--------------- Adminer -----------------";
    wget --progress=bar:force https://github.com/vrana/adminer/releases/download/v4.2.5/adminer-4.2.5-mysql-en.php;
    mv ./adminer-4.2.5-mysql-en.php ./adm.php;
    echo "--------------------------------------------------";
    cd ../;
    mysql -u root -e "CREATE DATABASE IF NOT EXISTS magento;";

    if [[ ! -z $SDV ]]
    then
      echo "--------------- Magento Sample Data -----------------";
      wget --progress=bar:force ${SOURCE_BASE_PATH}magento-sample-data-$SDV.zip;
      unzip -q magento-sample-data-$SDV.zip;
      rsync -qav magento-sample-data-$SDV/ web/;
      mysql -u root ldev < web/magento_sample_data_for_$SDV.sql;
      rm -rf magento-sample-data-$SDV magento-sample-data-$SDV.zip web/magento_sample_data_for_$SDV.sql;
    fi
  
    echo "--------------- Magento -----------------";
    wget --progress=bar:force ${SOURCE_BASE_PATH}magento-$MV.zip;
    unzip -q magento-$MV.zip;
    rsync -qav magento/ web/;
    rm -rf ./magento magento-$MV.zip;
    echo "--------------------------------------------------";

    chown vagrant:vagrant -R /var/www/l.dev/;
    chmod -R 0777 "/var/www/l.dev";
    service apache2 restart;
  SHELL
end
